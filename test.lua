-- this is a comment

--[[
    This is a multi-line comment
--]]


-- variables are global by default
age = 28

-- Print something to stdout, ommiting new line
io.write(">> Name: ")
-- get netxt stdin line
local name = io.read()

function check_name(somename)
    if somename == "derp" then
        -- you can also use print(), adds newline
        response = "herp derp"
    else
        -- ".." is the string concat operator
        response = somename .. " is " .. age .. " years old."
    end
    return response
end

print(check_name(name))

this_is_true = true

if this_is_true then print("yep, it's true.") end -- one-line logic

-- for i in range between 1 and 3 inclusively
print(">> Counting!")
start_zero = 0
for i = 1, 3 do
    start_zero = start_zero + 1
    print(start_zero)
end

print(">> Count down!")

repeat
    print(start_zero)
    start_zero = start_zero - 1
until start_zero == 0

function get_name()
    io.write(">> Okay, but what's your REAL name? ")
    dat_name = io.read()
    return dat_name
end

real_name = get_name()
print(check_name(real_name))

-- multiple assignment, 4 is dropped here
x, y, z = 1, 2, 3, 4

-- Tables!
-- Tables are the dictionaries of Lua. Also, the only compound data structure.

dict = { key1 = 1, key2 = 2 }
dict.key3 = 3
-- print("Key 1: " .. dict.key1)

function print_table_items(somedict)
    for key, val in pairs(dict) do
        -- table iteration
        print("KEY: " .. key .. ", VAL: " .. val)
    end
end

print(">> Print all key/value pairs in table:")
print_table_items(dict)

-- tables are also used as lists/arrays

-- list literals implicitly set up int keys
list = { "one", "two", "three" }

function print_list_items(somelist)
    for i = 1, #list do -- #list is the size of list
        print(list[i])
    end
end

-- NOTE: a list is not a real type. just a table with consecutive
-- integer keys, treated as a list.
print(">> Printing all items in the list:")
print_list_items(list)


-- Import a module
local toolbox = require('tb')
toolbox.hello(real_name)
