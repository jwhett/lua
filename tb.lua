-- the "toolbox" module
TB = {}

local function wave()
    print("**waves at you**")
end

function TB.hello(name)
    print("Hello, " .. name .. "!")
    wave()
end

return TB
